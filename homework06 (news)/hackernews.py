from bottle import (
    route, run, template, request, redirect
)

from scraputils import get_news
from db import session, News
from bayes import NaiveBayesClassifier
from pprint import pprint as pp
from db import save_database
from operator import itemgetter


@route("/news")
def news_list():
    s = session()
    rows = s.query(News).filter(News.label == None).all()
    return template('news_template', rows=rows)



@route('/add_label/')
def add_label():
    label = request.query.label
    id = request.query.id
    s = session()
    items = s.query(News).filter(News.id == id).all()
    print(items)
    for item in items:
        item.label = label
    s.commit()
    redirect('/news')


@route('/update')
def update_news():
    dicts = get_news('https://news.ycombinator.com/', n_pages=2)
    save_database(dicts)
    redirect('/news')

@route('/recommendations')
def recommendations():
    rows = s.query(News).filter(News.label == None).all()
    news = []
    for row in rows:
        prediction = model.predict([row.title])
        news.append((row, prediction[0]))

    news.sort(key=itemgetter(1))
    return template('news_recommendations', rows=news)


def get_training_data():
    rows = s.query(News).filter(News.label != None).all()
    X_train = [row.title for row in rows]
    y_train = [row.label for row in rows]

    return X_train, y_train


if __name__ == '__main__':
    s = session()
    X_train, y_train = get_training_data()
    model = NaiveBayesClassifier()
    model.fit(X_train, y_train)
    run(host='localhost', port=8080)