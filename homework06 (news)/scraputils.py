import requests
from bs4 import BeautifulSoup
import re

def get_news(url, n_pages):
    """ Collect news from a given web page """
    news = []
    while n_pages:
        response = requests.get(url)
        soup = BeautifulSoup(response.text, "html.parser")
        news_list = extract_news(soup)
        next_page = extract_next_page(soup)
        url = "https://news.ycombinator.com/" + next_page.get('href')
        news.extend(news_list)
        n_pages -= 1
    return news


def extract_news(parser):
    """ Extract news from a given web page """
    news_list = []
    tbl = parser.table.findAll('table')[1]
    title = tbl.findAll('a', {'class': 'storylink'})
    author = tbl.findAll('a', {'class': 'hnuser'})
    points = tbl.findAll('span', {'class': 'score'})
    for i in range(len(author)):
        news_list.append({'author': author[i].text,
                          'points': int(re.findall('(\d+)', points[i].text)[0]),
                          'title': title[i].text,
                          'url': title[i].get('href')
                         })
    return news_list

def extract_next_page(parser):
    """ Extract next page URL """
    next_page = parser.find('a', {'class': 'morelink'})
    return next_page
