def encrypt_vigenere(plaintext, keyword):
    """
    >>> encrypt_vigenere("PYTHON","A")
    'PYTHON'
    >>> encrypt_vigenere("python","a")
    'python'
    >>> encrypt_vigenere("ATTACKATDAWN", "LEMON")
    'LXFOPVEFRNHR'
    >>> encrypt_vigenere("PyThon","LEMON")
    'AcFvby'
    """
    ciphertext=''
    sp1=[]
    sp2=[]
    while len(plaintext)>len(keyword):
        keyword=keyword+keyword
    for i in keyword:
        c=ord(i)
        sp1.append(c)
    for i in plaintext:
        c=ord(i)
        sp2.append(c)
    c=len(plaintext)
    for i in range(0,c):
        if (sp1[i]>96 and sp2[i]<96):
            sp1[i]=sp1[i]-32
        if (sp1[i]<96 and sp2[i]>96):
            sp1[i]=sp1[i]+32        
        if sp2[i]>96:
            if sp1[i]+sp2[i]-97>122:
                a=sp1[i]+sp2[i]-123
            else:
                a=sp1[i]+sp2[i]-97
            ciphertext=ciphertext+chr(a)
        else:
            if sp1[i]+sp2[i]-65>90:
                a=sp1[i]+sp2[i]-91
            else:
                a=sp1[i]+sp2[i]-65
            ciphertext=ciphertext+chr(a)       
    return ciphertext   


def decrypt_vigenere(ciphertext, keyword):
    """
    >>> decrypt_vigenere ("PYTHON","A")
    'PYTHON'
    >>> decrypt_vigenere ("python","a")
    'python'
    >>> decrypt_vigenere("LXFOPVEFRNHR", "LEMON")
    'ATTACKATDAWN'
    >>> decrypt_vigenere("AcFvby","LEMON")
    'PyThon'
    """
    plaintext=''
    sp3=[]
    sp4=[]
    while len(ciphertext)>len(keyword):
        keyword=keyword+keyword
    for i in keyword:
        c=ord(i)
        sp3.append(c)
    for i in ciphertext:
        c=ord(i)
        sp4.append(c)
    c=len(ciphertext)
    for i in range(0,c):
        if (sp3[i]>96 and sp4[i]<96):
            sp3[i]=sp3[i]-32
        if (sp3[i]<96 and sp4[i]>96):
            sp3[i]=sp3[i]+32               
        if sp4[i]<91:
            if sp4[i]-sp3[i]+65<65:
                a=sp4[i]-sp3[i]+91
            else:
                a=sp4[i]-sp3[i]+65
            plaintext=plaintext+chr(a)
        else:
            if sp4[i]-sp3[i]+97<97:
                a=sp4[i]-sp3[i]+123
            else:
                a=sp4[i]-sp3[i]+97
            plaintext=plaintext+chr(a)  
    return plaintext    

