def encrypt_caesar(plaintext):
    """
    >>> encrypt_caesar("PYTHON")
    'SBWKRQ'
    >>> encrypt_caesar("python")
    'sbwkrq'
    >>> encrypt_caesar("")
    ''
    >>> encrypt_caesar("python,")
    'sbwkrq,'
    """
    ciphertext=''
    for i in plaintext:
        a=ord(i)
        if (a>90 and a<97) or a<65 or a>122:
            pass        
        elif a>119 and a<123:
            a=122-a+97
        elif a>87 and a<91:
            a=90-a+65
        else: 
            a=a+3
        ciphertext=ciphertext+chr(a)
    return (ciphertext)


def decrypt_caesar (ciphertext):
    """
    >>> decrypt_caesar("SBWKRQ")
    'PYTHON'
    >>> decrypt_caesar("sbwkrq")
    'python'
    >>> decrypt_caesar("")
    ''
    >>> decrypt_caesar("sbwkrq,")
    'python,'
    """
    plaintext=''
    for i in ciphertext:
        a=ord(i)
        if (a>90 and a<97) or a<65 or a>122:
            pass 
        elif a>64 and a<68:
            a=a-65+88
        elif a>96 and a<100:
            a=a-97+120
        else:
            a=a-3
        plaintext=plaintext+chr(a)
    return (plaintext)


def encrypt_caesar_shift(plaintext, shift):
    """
    >>> encrypt_caesar_shift("PYTHON",3)
    'SBWKRQ'
    >>> encrypt_caesar_shift("python",3)
    'sbwkrq'
    >>> encrypt_caesar_shift("",3)
    ''
    >>> encrypt_caesar_shift("python,",3)
    'sbwkrq,'
    """
    ciphertext=''
    for i in plaintext:
        a=ord(i)
        if (a>90 and a<97) or a<65 or a>122:
            pass
        elif a+shift>122:
            a=a-122+shift+96
        elif a+shift>90 and a<91:
            a=a-90+shift+64
        else: 
            a=a+shift
        ciphertext=ciphertext+chr(a)
    return (ciphertext)


def decrypt_caesar_shift(ciphertext, shift):
    """
    >>> decrypt_caesar_shift("SBWKRQ",3)
    'PYTHON'
    >>> decrypt_caesar_shift("sbwkrq",3)
    'python'
    >>> decrypt_caesar_shift("",3)
    ''
    >>> decrypt_caesar_shift("sbwkrq,",3)
    'python,'
    """
    plaintext=''
    for i in ciphertext:
        a=ord(i)
        if (a>90 and a<97) or a<65 or a>122:
            pass 
        elif a-shift<65:
            a=91-shift+(a-65)
        elif a-shift<97 and a>96:
            a=123-shift+(a-97)
        else:
            a=a-shift
        plaintext=plaintext+chr(a)
    return (plaintext)

